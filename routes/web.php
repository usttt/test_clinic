<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(
    [
        'namespace'     => 'SuperAdmin',
        'middleware' => ['web', 'auth', 'role:super_admin']
    ],
    function(){

        Route::get('/', 'HomeController@index');

        Route::resource('/owners', 'OwnersController');

    });

Route::group(
    [
        'namespace'     => 'NetworkOwners',
        'middleware' => ['web', 'auth', 'role:network_owner|clinic_manager']
    ],
    function(){

        Route::resource('/clinics', 'ClinicsController');
        Route::resource('/clinic_manager', 'ClinicManagerController');
        Route::resource('/insurance_case', 'InsuranceCaseController');
    });

