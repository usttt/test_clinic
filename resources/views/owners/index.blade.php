@extends('layouts.partials.layout')
@section('header')
    Владельцы Сети
@endsection

@section('breadcrumb')
    @include('layouts.partials.templates.breadcrumb', ['breadcrumbs' => ['Владельцы Сети']])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Владельцы Сети</h3>


            <a href="{{ url('/owners/create') }}"  class="btn btn-success float-right ml-4">
                <b><i class="fa fa-plus"></i> </b> Добавить Запись
            </a>

        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Действие</th>
                    </tr>
                </thead>

            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@section('script')
    <script>
        initDatatable("{!!url('/owners') !!}" ,
            { data: 'id', name: 'id'},
            { data: "name", name: 'name'},
            { data: "phone", name: 'phone'},
            { data: 'action', name: 'action', orderable: false, searchable: false, width:400})
    </script>
@endsection
