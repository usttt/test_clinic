<?php

namespace App\Http\Controllers\NetworkOwners;

use App\Imports\InsuranceImport;
use App\Models\Clinics;
use App\Models\Insurance;
use App\Models\InsuranceCase;
use App\Models\InsuranceCaseRelation;
use App\Models\Patients;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class InsuranceCaseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){

            $insurance_case = InsuranceCase::with(['clinic', 'patient', 'insurance'])->orderBy('id', 'ASC');

            return DataTables::eloquent($insurance_case)
                ->addColumn('insurance', function ($insurance_case) {
                    return $insurance_case->insurance->map(function($insurance) {
                        return $insurance->name;
                    })->implode('<br>');
                })
                ->escapeColumns(null)
                ->make(true);
        }

        return view('insurance_case.index');
    }

    public function create()
    {
        $clinics = Clinics::all();

        if(in_array('clinic_manager', Auth::user()->getRoleNames()->toArray()))
        {
            $clinics = Clinics::where('manager_id', Auth::user()->id)->get();
        }

        return view('insurance_case.create', compact('clinics'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'clinic_id' => 'required|exists:clinics,id',
            'file' => 'required',
        ]);

        $clinic_id = $request->input('clinic_id');
        $file = Excel::toArray(new InsuranceImport(), request()->file('file'));

        $datas = [];
        $counter = 0;

        /*
         * Обрабатываем файл Excel и добавляем в новый массив
         */

        foreach ($file[1] as $key => $client_data)
        {
            if($client_data[0] && $client_data[1])
            {
                $counter = $counter+1;
                $datas[] = [ 'patient' => $client_data[1], 'phone' => $client_data[3]];
            }

            if(!strpos($client_data[1], ',') && is_string($client_data[1]))
            {

                $datas[$counter-1]['insurance'][] = $client_data[1];
            }

            if(is_int($client_data[1]) )
            {
                $datas[$counter-1]['insurance_case'][] =[
                    'price' => $client_data[5],
                    'date' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($client_data[1]))->format('d.m.Y'),
                    'name' => $client_data[4],
                ];
            }

        }

        /*
         * Записываем в базу данных
         */

        try {
            DB::beginTransaction();
            foreach ($datas as $data)
            {
                $patient = Patients::updateOrCreate([
                    'phone' => $data['phone'],
                    'name' => $data['patient']],
                    [
                        'phone' => $data['phone'],
                        'name' => $data['patient'],
                    ]);

                foreach ($data['insurance_case'] as $insurance_case)
                {
                    $case = InsuranceCase::create(
                        /*[
                        'patient_id' => $patient->id,
                    ],*/
                        [
                            'patient_id' => $patient->id,
                            'name' => $insurance_case['name'],
                            'date' => $insurance_case['date'],
                            'who_created' => Auth::user()->name,
                            'price' => $insurance_case['price'],
                            'status' => 'created',
                            'clinic_id' => $clinic_id
                        ]);

                    foreach ($data['insurance'] as $insurance)
                    {
                        $insurance_create = Insurance::updateOrCreate([
                            'name' => $insurance],
                            [
                                'name' => $insurance,
                            ]);

                        InsuranceCaseRelation::updateOrCreate([
                            'insurance_id' => $insurance_create->id,
                            'insurance_case_id' => $case->id
                        ],
                            [
                                'insurance_id' => $insurance_create->id,
                                'insurance_case_id' => $case->id
                            ]);

                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }

        return back()->with('message', 'Запись успешно добавлен');
    }

    public function edit($id)
    {
        $data = Clinics::find($id);

        return view('clinics.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|unique:clinics,phone,'.$id.'id',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');

        $clinic = Clinics::find($id);

        $clinic->name = $name;
        $clinic->phone = $phone;
        $clinic->address = $address;

        $clinic->save();

        return back()->with('message', 'Запись успешно изменен');
    }

    public function destroy($id)
    {
        $clinic = Clinics::find($id)->delete();

        return  Response()->json($clinic);
    }
}
