<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceCaseRelation extends Model
{
    protected $table = 'i_c_relation';

    protected $fillable = ['insurance_id', 'insurance_case_id'];

}
