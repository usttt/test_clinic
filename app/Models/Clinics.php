<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Clinics extends Model
{
    protected $table = 'clinics';

    protected $fillable = ['name', 'address', 'phone', 'manager_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'manager_id', 'id');
    }
}
