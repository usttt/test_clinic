<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = User::firstOrCreate(['phone' => '900900900'],[
            'name'		=> 'Admin',
            'phone'		=> '900900900',
            'password'	=> bcrypt('admin'),
        ]);

        $clinic_manager = User::firstOrCreate(['phone' => '700700700'],[
            'name'		=> 'Manager',
            'phone'		=> '700700700',
            'password'	=> bcrypt('manager'),
        ]);

        $network_owner = User::firstOrCreate(['phone' => '600600600'],[
            'name'		=> 'owner',
            'phone'		=> '600600600',
            'password'	=> bcrypt('00000000'),
        ]);

        $super_admin->assignRole('super_admin');
        $clinic_manager->assignRole('clinic_manager');
        $network_owner->assignRole('network_owner');
    }
}
