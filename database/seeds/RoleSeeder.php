<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate(['name' => 'super_admin'],['name' => 'super_admin']);

        Role::firstOrCreate(['name' => 'clinic_manager'],['name' => 'clinic_manager']);

        Role::firstOrCreate(['name' => 'network_owner'],['name' => 'network_owner']);

    }
}
